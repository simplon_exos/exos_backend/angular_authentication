import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { User } from '../user';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  user:User = {
    email:'',
    password: ''
  };
  confirm:string;
  userExists = false;

  @Output()
  registered = new EventEmitter();

  constructor(private authService:AuthService) { }

  ngOnInit() {
  }

  register() {
    this.authService.addUser(this.user).subscribe(
      () => this.registered.emit()
    );
  }

  checkEmail() {
    console.log('bloup');
    this.authService.emailExists(this.user.email).subscribe(
      () => this.userExists = true,
      () => this.userExists = false
    );
  }
}
