import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from './user';
import { tap, switchMap } from "rxjs/operators";
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private url = 'http://localhost:8000/api/user';
  /**
   * Un Subject est un Observable spécial sur lequel on va à la fois
   * pour se subscribe, comme un Observable classique, mais également 
   * pouvoir pousser de nouvelle valeur à l'intérieur. Ici on s'en sert
   * pour avoir le user actuellement connecté qui évoluera dans le
   * temps au fil des connexions/déconnexions
   */
  public user:BehaviorSubject<User> = new BehaviorSubject(null);

  constructor(private http:HttpClient) {}
  /**
   * La méthode addUser permet de créer un nouveau user sur le serveur
   */
  addUser(user:User) {
    return this.http.post<User>(this.url, user);
  }
  /**
   * La méthode login va faire la requête vers l'api et récupérer le token.
   * On fait ici en sorte de déclencher la méthode getUser() au moment de
   * récupérer le token afin d'avoir directement le user en plus du token
   * au moment de la connexion
   */
  login(username:string, password:string) {
    return this.http.post<{token:string}>('http://localhost:8000/api/login_check', {
      username,
      password
    }).pipe(
      //On utilise le tap pour stocker le token en localStorage lorsque la
      //réponse http arrivera avec succès
      tap(response => localStorage.setItem('token', response.token)),
      //on utilise le switchMap pour déclencher la méthode getUser au moment
      //où la réponse http arrivera avec succès
      switchMap(() => this.getUser())
    );
  }
  
  /**
   * La méthode logout permet de se déconnecter
   */
  logout() {
    //Ici la déconnexion consiste en la suppression du token du localstorage
    localStorage.removeItem('token');
    //Et à pousser une valeur null dans le Subject<User>
    this.user.next(null);
  }
  /**
   * La méthode getUser fait un appel ajax pour récupérer le user 
   * correspondant au token actuel
   */
  getUser(): Observable<User> {
    return this.http.get<User>(this.url).pipe(
      //On utilise l'opérateur pipe pour faire en sorte de pousser le user
      //dans le Subject au moment où l'observable récupère la réponse http
      tap(user => this.user.next(user))
    );
  }

  emailExists(email:string) {
    return this.http.get<void>(this.url + '/' + email);
  }
}
